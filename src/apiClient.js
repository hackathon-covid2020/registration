import axios from 'axios'
import {notification} from 'antd'
import {BASE_URL} from './components/const'

export const createMuseum = payload => {
    try {
       return axios.post(`${BASE_URL}/api/museums`, payload)
            .then((response) => {
                notification['success']({
                    message: 'Sucesso!',
                    description: 'Informações enviadas com sucesso.',
                })
                return response.data.id
            }, (error) => {
                notification['error']({
                    message: 'Erro!',
                    description: 'Houve um problema ao enviar os dados, tente novamente.',
                })
            })
    } catch(err) {
        notification['error']({
            message: 'Erro!',
            description: 'Houve um problema ao enviar os dados, tente novamente.',
        })
    }
}

export const createExhibition = payload => {
    try {
       return axios.post(`${BASE_URL}/api/exhibitions`, payload)
            .then((response) => {
                notification['success']({
                    message: 'Sucesso!',
                    description: 'Informações enviadas com sucesso.',
                })
                return response.data.id
            }, (error) => {
                notification['error']({
                    message: 'Erro!',
                    description: 'Houve um problema ao enviar os dados, tente novamente.',
                })
            })
    } catch(err) {
        notification['error']({
            message: 'Erro!',
            description: 'Houve um problema ao enviar os dados, tente novamente.',
        })
    }
}

export const createCollection = payload => {
    try {
       return axios.post(`${BASE_URL}/api/collections`, payload)
            .then((response) => {
                notification['success']({
                    message: 'Sucesso!',
                    description: 'Informações enviadas com sucesso.',
                })
                return response.data.id
            }, (error) => {
                notification['error']({
                    message: 'Erro!',
                    description: 'Houve um problema ao enviar os dados, tente novamente.',
                })
            })
    } catch(err) {
        notification['error']({
            message: 'Erro!',
            description: 'Houve um problema ao enviar os dados, tente novamente.',
        })
    }
}

export const uploadImage = (appendImgUrl, key) => ({onSuccess, onError, file}) => {
    const formData = new FormData();
    formData.append("collection-photo", file)

    try {    
        axios.post(`${BASE_URL}/api/upload`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            notification['success']({
                message: 'Sucesso!',
                description: 'Upload concluído com sucesso.',
            })
            onSuccess()
            console.log('api response image url: ', response.data.imageUrl)
            appendImgUrl(key, response.data.imageUrl)
        }, (error) => {
            notification['error']({
                message: 'Erro!',
                description: 'Houve um problema ao enviar a imagem, tente novamente.',
            })
            onError()
        })
    } catch(err) {
        notification['error']({
            message: 'Erro!',
            description: 'Houve um problema ao enviar a imagem, tente novamente.',
        })
    }
}
