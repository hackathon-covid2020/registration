import React, {useState, useEffect} from "react"
//import { Link } from "gatsby"
import firebase from "gatsby-plugin-firebase"
import Layout from "../components/layout"
import Registration from "../components/registration"
import Welcome from "../components/welcome"
import { useAuthState } from 'react-firebase-hooks/auth';

const IndexPage = () => {
    const [view, setView] = useState("welcome")
    const [redirected, setRedirected] = useState(false)

    const signIn = () => {
        const provider = new firebase.auth.GoogleAuthProvider()
        firebase.auth().signInWithRedirect(provider).then(result => {
            const token = result.credential.accessToken
            const user = result.user
        }).catch(error => {
            console.log(error.message)
        })
    }

    const { auth } = firebase;
    const [user, setUser] = useState();
    useEffect(() => auth().onAuthStateChanged(setUser), [])
    
    //const [user, loading, error] = useAuthState(firebase.auth())
    const action = user ? (() => {setView("registration")}) : signIn
    
    const views = {
        "welcome": <Welcome proceed={action} />,
        "registration": <Registration user={user ? user.email : 'unauthorized'}/>
    }
    
    return (
        <Layout>
          {views[view]}
        </Layout>
    )
}

export default IndexPage
