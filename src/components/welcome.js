import React, {useState} from 'react'
import styled from 'styled-components'
import { Result, Button, Card, Form, Input } from 'antd';
import { BankTwoTone, GoogleOutlined } from '@ant-design/icons';
import { useAuthState } from 'react-gatsby-firebase-hooks/auth';
import firebase from "gatsby-plugin-firebase"
import {notification} from 'antd'

const Root = styled.div`
  margin-top: 100px;
  width: 80vw;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Welcome = (props) => {
    const [user, loading, error] = useAuthState(firebase);

    return (
        <Root>
          <Card>
            <Result
              icon={<BankTwoTone twoToneColor="#52c41a" />}
              title="Welcome to Museums Cloud registration page!"
              subTitle="Follow the steps to make your museum visible to the world."
              extra={[
                  <Button
                    disabled={loading}
                    loading={loading}
                    type="primary"
                    key="console"
                    {...(user ? {} : {icon: <GoogleOutlined/>})}
                    size="large"
                    onClick={props.proceed}
                  >
                    {user ? "Continue" : "Sign in with Google"}
                  </Button>,
              ]}
            />
          </Card>
        </Root>
    )
}

export default Welcome
