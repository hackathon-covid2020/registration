import React, { useState } from 'react'
import styled from 'styled-components'
import Uploader from './upload'
import * as Api from '../apiClient'
import { nanoid } from 'nanoid'
import countryList from 'react-select-country-list'
import {
    MinusCircleOutlined,
    PlusOutlined,
    DeleteOutlined
} from '@ant-design/icons';

import {
    AutoComplete,
    Form,
    Input,
    Button,
    Card,
    Select,
    DatePicker,
    Space,
    Modal,
    notification,
} from 'antd';

const { TextArea } = Input;

const Root = styled.div`
  margin-top: 50px;
  width: 60vw;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const CollectionForm = (props) => {
    const [form] = Form.useForm()
    const [confirm, setConfirm] = useState(false)
    
    React.useEffect(() => {
        form.setFieldsValue({
            items: [{
                title: '',
                author: '',
                kind: '',
                country: '',
                productionDate: '',
                acquisitionDate: '',
            }],
        });
    }, []);

    const [imgContainer, setImgContainer] = useState({})

    const [lockButton, setLockButton] = useState(false) 
    const onFinish = coll => {
        setLockButton(true)

        coll['museumId'] = props.museumId
        coll['museumName'] = props.museumName

        for(let key in imgContainer) {
            coll.items[key].imageUrl = imgContainer[key]
        }
        props.setCollection(coll.items)
        
        Api.createCollection(coll).then(id => {
            props.nextPage()
        })
    }
    
    return (
        <Root>
          <Modal
            visible={confirm}
            title="Are you sure to proceed?"
            onOk={props.nextPage}
            onCancel={() => setConfirm(false)}
          >
            <p>Check the provided information.<br/>
              You will not be able to edit this after submit.
            </p>
          </Modal>
          <Form
            style={{width: '70%'}}
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 15 }}
            layout="horizontal"
            size="large"
            labelAlign="left"
            onFinish={onFinish}
          >
            <Form.List name="items">
              {(fields, { add, remove }) => {
                  return (
                      <div>
                        {fields.map(field => (
                            <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="start">
                              <Card
                                title="New Collection Item" style={{ width: '42vw' }}
                                actions={[
                                    <Button
                                      style={{width: "100%"}}
                                      size="small"
                                      type="text"
                                      onClick={() => {remove(field.name)}}
                                    >
                                      <DeleteOutlined />
                                    </Button>
                                ]}
                              >
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'title']}
                                  fieldKey={[field.fieldKey, 'title']}
                                  label="Title"
                                  rules={[{ required: true, message: 'this is a required field!'}]}
                                >
                                  <Input />
                                </Form.Item>
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'author']}
                                  fieldKey={[field.fieldKey, 'author']}
                                  label="Author"                                  
                                >
                                  <Input />
                                </Form.Item>
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'kind']}
                                  fieldKey={[field.fieldKey, 'kind']}                                  
                                  label="Artifact type"
                                  rules={[{ required: true, message: 'this is a required field!'}]}
                                >
                                  <Select>
                                    <Select.Option value="PAINTING">Painting</Select.Option>
                                    <Select.Option value="SCULPTURE">Sculpture</Select.Option>
                                    <Select.Option value="DOCUMENT">Document</Select.Option>
                                    <Select.Option value="CURRENCY">Currency</Select.Option>
                                    <Select.Option value="ARCHEALOGICAL">Archealogical</Select.Option>
                                    <Select.Option value="OTHER">Other</Select.Option>
                                  </Select>
                                </Form.Item>        
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'country']}
                                  fieldKey={[field.fieldKey, 'country']}
                                  label="Origin country"
                                >
                                  <Select options={countryList().getData()}/>
                                </Form.Item>
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'productionDate']}
                                  fieldKey={[field.fieldKey, 'productionDate']}                                  
                                  label="Production Date"
                                >
                                  <DatePicker />
                                </Form.Item>
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'acquisitionDate']}
                                  fieldKey={[field.fieldKey, 'acquisitionDate']}                                  
                                  label="Acquisition Date"
                                >
                                  <DatePicker />
                                </Form.Item>
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'image']}
                                  fieldKey={[field.fieldKey, 'image']}
                                  label="Image"                                  
                                  required="true"
                                >
                                  <Uploader
                                    title={field.name}
                                    imgContainer={imgContainer}
                                    setImgContainer={setImgContainer}                                    
                                  />
                                </Form.Item>              
                              </Card>
                            </Space>
                        ))}
                        <Form.Item>
                          <Button
                            style={{width: '42vw'}}
                            type="dashed"
                            onClick={() => {add()}}
                            block
                          >
                            <PlusOutlined /> New Item
                          </Button>
                        </Form.Item>
                      </div>
                  )         
              }}
            </Form.List>
            
            <Form.Item>
              <Button
                disabled={lockButton}
                loading={lockButton}
                size="large"
                type="primary"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Root>
    );
};

export default CollectionForm
