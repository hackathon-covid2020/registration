import React, {useState} from 'react'
import { Upload, message } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import {BASE_URL} from './const'
import {uploadImage} from '../apiClient'

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!');
    }

    return isJpgOrPng;
}

const Uploader = (props) => {
    const [state, setState] = useState({
        image: '',
        loading: false
    })

    const appendImgUrl = (key, val) => {
        const tmp = {...props.imgContainer}
        tmp[key] = val
        props.setImgContainer(tmp)
    }

    const handleChange = info => {
        if (info.file.status === 'uploading') {
            setState({ ...state, loading: true });
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl => {
                setState({
                    ...state,                    
                    imageUrl,
                    loading: false,
                })
            })
        }
    };

    const uploadButton = (
        <div>
          {state.loading ? <LoadingOutlined /> : <PlusOutlined />}
          <div className="ant-upload-text">Upload</div>
        </div>
    );
    const { imageUrl } = state;
    const action = `${BASE_URL}/api/upload`

    return (
        <Upload
          name="collection-photo"
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          beforeUpload={beforeUpload}
          onChange={handleChange}
          customRequest={uploadImage(appendImgUrl, props.title)}
        >
          {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
        </Upload>
    );
}


export default Uploader
