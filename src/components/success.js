import React, { useState } from 'react'
import styled from 'styled-components'
import * as Api from '../apiClient'
import { SmileTwoTone } from '@ant-design/icons';
import { navigate } from "gatsby"
import {
    Button,
    Result,
} from 'antd';

const Root = styled.div`
  margin-top: 50px;
  width: 60vw;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Success = (props) => {    
    return (
        <Root>
          <Result
            icon={<SmileTwoTone twoToneColor="#52c41a" />}
            title="Succesfully saved your collection!"
            subTitle="Click the button below to proceed to Museums Cloud website"
            status='success'
            extra={<Button onClick={() => window.location.href = "https://museums.cloud"} type="primary">Proceed</Button>}
          />
        </Root>
    )
}

export default Success
