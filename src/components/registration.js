import React, {useState} from 'react'
import styled from 'styled-components'

import RegistrationProgress from './progress'
import MuseumForm from './museumForm'
import CollectionForm from './collectionForm'
import ExhibitionForm from './exhibitionForm'
import Success from './success'

const Root = styled.div`
  width: 80vw;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Registration = (props) => {
    const [progressIdx, setProgressIdx] = useState(0)
    const nextPage = () => setProgressIdx(progressIdx + 1)
    const previousPage = () => setProgressIdx(progressIdx - 1)
    const [museumId, setMuseumId] = useState('')
    const [museumName, setMuseumName] = useState('')
    const [collection, setCollection] = useState({})
    
    const idxMap = {
        0: <MuseumForm
             owner={props.user}
             setMuseumId={setMuseumId}
             setMuseumName={setMuseumName}
             nextPage={nextPage}
           />,
        1: <CollectionForm
             museumId={museumId}
             museumName={museumName}
             collection={collection}
             setCollection={setCollection}
             nextPage={nextPage}
           />,
        2: <ExhibitionForm
             museumId={museumId}
             museumName={museumName}
             collection={collection}
             setCollection={setCollection}             
             nextPage={nextPage}
           />,
        3: <Success/>
    }
    
    return (
        <Root>
          <RegistrationProgress progressIdx={progressIdx}/>
          {idxMap[progressIdx]}
        </Root>
    )
}

export default Registration
