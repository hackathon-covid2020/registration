import React from "react"
import styled, { createGlobalStyle } from 'styled-components'

import Header from "./header"
import "./layout.css"

const GlobalStyle = createGlobalStyle`
html {
  font-family: Roboto;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}

body {
  margin: 0 auto;
  height: 100vh;
  width: 100vw;
  margin: 0 auto;
  display: flex;
  justify-content: center;

  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
`

const Root = styled.div`
  margin-top: 30px;
  width: 100vw;
  display: flex;
  align-items: center;
  flex-direction: column;
`

const Layout = ({ children }) => {
  return (
      <Root>
        <GlobalStyle />
        <main>{children}</main>
      </Root>
  )
}

export default Layout
