import React, { useState } from 'react'
import styled from 'styled-components'
import Uploader from './upload'
import * as Api from '../apiClient'
import { nanoid } from 'nanoid'
import countryList from 'react-select-country-list'
import {
    MinusCircleOutlined,
    PlusOutlined,
    DeleteOutlined
} from '@ant-design/icons';

import {
    AutoComplete,
    Form,
    Input,
    Button,
    Card,
    Select,
    DatePicker,
    Space,
    Modal,
    notification,
    Radio,
} from 'antd';

const { TextArea } = Input;
const Root = styled.div`
  margin-top: 50px;
  width: 60vw;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const ExhibitionForm = (props) => {
    const [form] = Form.useForm();

    React.useEffect(() => {
        form.setFieldsValue({
          museumId: "",
          museumName: "",
          title:  "",
          startDate: "",
          catalogue: []
        });
    }, []);

    const [lockButton, setLockButton] = useState(false) 
    const onFinish = coll => {
        setLockButton(true)

        coll.catalogue.unshift({
          informationBit: coll.informationBit,
          theme: coll.theme,
          backgroundImage: "https://museums-cloud-images.s3.us-south.cloud-object-storage.appdomain.cloud/9047e8b7-6a95-4378-97ab-4634f4df4695.png",
          backgroundColor: "#ffffff",
        })

        coll.catalogue.push({
          informationBit: coll.endingText,
          dotaneLink: coll.donateLink,
          theme: coll.closingTheme,
          backgroundColor: "#ffffff",
          backgroundImage: "https://museums-cloud-images.s3.us-south.cloud-object-storage.appdomain.cloud/9047e8b7-6a95-4378-97ab-4634f4df4695.png",
        })

        coll.museumName = props.museumName
        coll.museumId = props.museumId

        delete coll.informationBit
        delete coll.theme
        delete coll.backgroundColor
        delete coll.backgroundImage

        delete coll.endingText
        delete coll.donateLink
        delete coll.closingTheme
        delete coll.endingBackgroundColor
        delete coll.endingBackgroundImage

        console.log('print do role: ', coll)

        Api.createExhibition(coll).then(id => {
            props.nextPage()
        })
    }
    
    return (
        <Root>
          <Form
            form={form}
            style={{width: '70%'}}
            labelCol={{ span: 6 }}
            wrapperCol={{ span: 12 }}
            layout="horizontal"
            size="large"
            labelAlign="left"
            onFinish={onFinish}
          >
            <Space style={{ display: 'flex', marginBottom: 8 }} align="start">
              <Card
                title="New Exhibition" style={{ width: '42vw' }}
              >
                <Form.Item
                  name='title'
                  label="Exhibition name"
                  rules={[{ required: true, message: 'this is a required field!'}]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name='museumId'
                  label="Exhibition name"
                  style={{display: 'none'}}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name='museumName'
                  label="Exhibition name"
                  style={{display: 'none'}}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name='startDate'
                  label="Exhibition name"
                  style={{display: 'none'}}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name='informationBit'
                  label="Introductory text"                                  
                >
                  <TextArea rows={5}/>
                </Form.Item>
                <Form.Item
                  name='backgroundColor'
                  label="Background Color"
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="backgroundImage"
                  label="Background Image"
                >
                <Uploader />
              </Form.Item>    
              <Form.Item
                  name='theme'
                  label="Theme"
                >
                <Radio.Group
                  options={
                    [
                      { label: 'Light', value: 'light' },
                      { label: 'Dark', value: 'dark' },
                    ]
                  }
                  optionType="button"
                />
                </Form.Item>
              </Card>
            </Space>

            <Form.List name="catalogue">
              {(fields, { add, remove }) => {
                  return (
                      <div>
                        {fields.map(field => (
                            <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="start">
                              <Card
                                title={"New Item" + field.key} style={{ width: '42vw' }}
                                actions={[
                                    <Button
                                      style={{width: "100%"}}
                                      size="small"
                                      type="text"
                                      onClick={() => {remove(field.name)}}
                                    >
                                      <DeleteOutlined />
                                    </Button>
                                ]}
                              >
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'author']}
                                  fieldKey={[field.fieldKey, 'author']}                                  
                                  label="Author"
                                  style={{display: 'none'}}
                                >
                                  <Input />
                                </Form.Item>
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'imageUrl']}
                                  fieldKey={[field.fieldKey, 'productionYear']}                                  
                                  label="productionYear"
                                  style={{display: 'none'}}
                                >
                                  <Input />
                                </Form.Item>
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'productionYear']}
                                  fieldKey={[field.fieldKey, 'productionYear']}                                  
                                  label="productionYear"
                                  style={{display: 'none'}}
                                >
                                  <Input />
                                </Form.Item>
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'title']}
                                  fieldKey={[field.fieldKey, 'item']}                                  
                                  label="Collection Item"
                                  rules={[{ required: true, message: 'this is a required field!'}]}
                                >
                                  <Select>
                                    {props.collection.map(item => (                                        
                                        <Select.Option value={item.title}>{item.title}</Select.Option>
                                    ))}
                                  </Select>
                                </Form.Item>
                                <Form.Item
                                  {...field}
                                  name={[field.name, 'informationBit']}
                                  fieldKey={[field.fieldKey, 'description']}                                  
                                  label="Description"
                                  rules={[{ required: true, message: 'this is a required field!'}]}
                                >
                                  <TextArea rows={5}/>
                                </Form.Item>
                                <Form.Item
                                  name={[field.name, 'backgroundColor']}
                                  fieldKey={[field.fieldKey, 'backgroundColor']}                                  
                                  label="Background Color"
                                >
                                  <Input />
                                </Form.Item>
                                <Form.Item
                                  name={[field.name, 'backgroundImage']}
                                  fieldKey={[field.fieldKey, 'backgroundImage']}                                  
                                  label="Background Image"
                                >
                                  <Uploader />
                                </Form.Item>
                                <Form.Item
                                  name={[field.name, 'theme']}
                                  fieldKey={[field.fieldKey, 'theme']}                                  
                                  label="Theme"
                                >
                                <Radio.Group
                                  options={
                                    [
                                      { label: 'Light', value: 'light' },
                                      { label: 'Dark', value: 'dark' },
                                    ]
                                  }
                                  optionType="button"
                                />
                                </Form.Item>            
                              </Card>
                            </Space>
                        ))}
                        <Form.Item>
                          <Button
                            style={{width: '42vw'}}
                            type="dashed"
                            onClick={() => {add()}}
                            block
                          >
                            <PlusOutlined /> New Item
                          </Button>
                        </Form.Item>
                      </div>
                  )         
              }}
            </Form.List>
            <Space style={{ display: 'flex', marginBottom: 8 }} align="start">

              <Card
                title="Ending" style={{ width: '42vw' }}
              >
                <Form.Item
                  name='donateLink'
                  label="donateLink"
                  style={{display: 'none'}}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name='endingText'
                  label="Ending text"                                  
                >
                  <TextArea rows={5}/>
                </Form.Item>
                <Form.Item
                  name='endingBackgroundColor'
                  label="Background Color"
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="endingBackgroundImage"
                  label="Background Image"
                >
                <Uploader />
              </Form.Item>    
              <Form.Item
                  name='closingTheme'
                  label="Theme"
                >
                <Radio.Group
                  options={
                    [
                      { label: 'Light', value: 'light' },
                      { label: 'Dark', value: 'dark' },
                    ]
                  }
                  optionType="button"
                />
                </Form.Item>
              </Card>
            </Space>

            <Form.Item>
              <Button
                disabled={lockButton}
                loading={lockButton}
                size="large"
                type="primary"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Root>
    );
};

export default ExhibitionForm
