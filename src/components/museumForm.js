import React, { useState } from 'react'
import styled from 'styled-components'
import * as Api from '../apiClient'

import {
    Form,
    Input,
    Button,
    Space,
    Card,
    Select,
    DatePicker,
    notification,
} from 'antd';

const { TextArea } = Input;

const Root = styled.div`
  margin-top: 50px;
  width: 60vw;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const MuseumForm = (props) => {
    const [form] = Form.useForm();

    React.useEffect(() => {
        form.setFieldsValue({
            name: '',
            kind: '',
            address: '',
            website: '',
            contactEmail: '',
            dateOfEstablishment: '',
            shortDescription: '',
            bio: '',
        });
    }, []);

    const [lockButton, setLockButton] = useState(false) 
    const onFinish = museum => {
        setLockButton(true)
        museum['owner'] = props.owner
        Api.createMuseum(museum).then(id => {
            props.setMuseumId(id)
            props.setMuseumName(museum.name)
            props.nextPage()
        })
    }
    
    return (
        <Root>
          <Form
            style={{width: '70%'}}
            labelCol={{ span: 6 }}
            wrapperCol={{ span: 15 }}
            layout="horizontal"
            size="large"
            labelAlign="left"
            onFinish={onFinish}
          >
            <Space style={{ display: 'flex', marginBottom: 8 }} align="start">
              <Card
                title="Museum Information" style={{ width: '42vw' }}
              >
                <Form.Item
                  name="name"
                  label="Museum's name"
                  rules={[{ required: true, message: "this is a required field!" }]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="kind"
                  label="Museum's Kind"
                  rules={[{ required: true, message: "this is a required field!"}]}              
                >
                  <Select>
                    <Select.Option value="ARTS">Arts</Select.Option>
                    <Select.Option value="HISTORY">History</Select.Option>
                    <Select.Option value="SCIENCES">Sciences</Select.Option>
                    <Select.Option value="OTHERS">Others</Select.Option>
                  </Select>
                </Form.Item>
                <Form.Item
                  name="address"
                  label="Address"
                >
                  <Input/>
                </Form.Item>
                <Form.Item
                  name="website"
                  label="Website"
                >
                  <Input/>
                </Form.Item>
                <Form.Item
                  name="contactEmail"
                  label="Email contact"
                  rules={[{type: 'email', message: 'it must be a valid email',}]}
                >
                  <Input/>
                </Form.Item>
                <Form.Item
                  name="dateOfEstablishment"
                  label="Date of establishment"
                  rules={[{ required: true, message: 'this is a required field!' }]}
                >
                  <DatePicker />
                </Form.Item>
                <Form.Item
                  name="shortDescription"
                  label="Short Description"
                  rules={[{ required: true, message: 'this is a required field!' }]}              
                >
                  <Input/>
                </Form.Item>
                <Form.Item
                  name="bio"
                  label="Bio"
                  rules={[{ required: true, message: 'this is a required field!'}]}              
                >
                  <TextArea rows={5}/>
                </Form.Item>
              </Card>
            </Space>
            
            <Form.Item>
              <Button
                disabled={lockButton}
                loading={lockButton}
                type="primary"
                size='large'
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Root>
    );
};

export default MuseumForm;
