import React from 'react'
import styled from 'styled-components'

import { Steps } from 'antd'
const { Step } = Steps;

const Root = styled.div`
  width: 42vw;
`

const RegistrationProgress = (props) => (
    <Root>
      <Steps current={props.progressIdx}>
        <Step title="Registration" />
        <Step title="Add Collection Items" />
        <Step title="Create Exhibition" />
      </Steps>
    </Root>
)

export default RegistrationProgress
