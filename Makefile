dev:
	@gatsby develop -p 8000 -H 0.0.0.0

deploy:
	@gatsby build
	@wrangler publish --env production
